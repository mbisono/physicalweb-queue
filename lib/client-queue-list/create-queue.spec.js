/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    createQueueFn = require('./create-queue');
chai.use(chaiAsPromised);

describe("create-queue.js", function () {
    var queueList = require('../client-queue-list');
    var createQueue = createQueueFn(queueList);

    it('Debe instanciarse correctamente', function () {
        expect(createQueueFn).to.be.a('function');
        expect(createQueue).to.be.a('function');
    });

    it("Debe crear una cola nueva en el listado de colas", function () {
        expect(queueList.members.length).to.equal(0);
        createQueue();
        expect(queueList.members.length).to.equal(1);
        createQueue();
        expect(queueList.members.length).to.equal(2);
        createQueue();
        expect(queueList.members.length).to.equal(3);
    });
});
