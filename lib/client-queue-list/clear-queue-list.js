/**
 * Created by mbisono on 3/2/16.
 */
var Promise = require('bluebird');

module.exports = function (queueList) {
    return function () {
        return new Promise(function (resolve) {
            queueList.members = [];
            resolve(queueList);
        });
    };
};
