/**
 * Created by mbisono on 3/2/16.
 */
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = function (queueList) {
    "use strict";
    return function (id) {
        return new Promise(function (resolve, reject) {
            var queue = _.find(queueList.members, function (queue) {
                return queue.id === id;
            });
            
            if (!queue) { throw  "La cola no existe"; }
            
            return resolve(queue);
        });
    };
};
