/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

describe('clear-queue-list', function () {
    var queueList = require('../client-queue-list');

    it("Debe construirse correctamente", function () {
        expect(queueList).to.be.a('object');
    });

    expect('Debe tener una propiedad members', function () {
        expect(queueList.members).to.be.a('array');
        expect(queueList.members.length).to.equal(0);
    });

    describe("createQueue()", function () {
        before(function () {
            queueList.clearQueueList();
        });

        it("Debe crear una cola nueva en la lista", function () {
            expect(queueList.members.length).to.equal(0);
            queueList.createQueue();
            expect(queueList.members.length).to.equal(1);
            queueList.createQueue();
            expect(queueList.members.length).to.equal(2);
            queueList.createQueue();
            expect(queueList.members.length).to.equal(3);
        });

        after(function () {
            queueList.clearQueueList();
        });
    });

    describe("clearQueueList()", function () {
        before(function () {
           queueList.createQueue(); 
           queueList.createQueue(); 
           queueList.createQueue(); 
           queueList.createQueue(); 
        });
        
        it('debe limpiar el listado de colas.', function () {
            expect(queueList.members.length).to.be.above(0);
            queueList.clearQueueList();
            expect(queueList.members.length).to.equal(0);
        });
    });

    describe('findById()', function () {
        before(function () {
            queueList.createQueue();
            queueList.createQueue();
            queueList.createQueue();
            queueList.createQueue();
        });
        
        it("debe traer la cola con el id indicado", function (done) {
            expect(queueList.members.length).to.be.above(0);
            var id = queueList.members[2].id;
            queueList.findById(id).then(function (queue) {
                expect(queue).to.be.equal(queueList.members[2]);
                done();
            });
            
        });
    });

});
