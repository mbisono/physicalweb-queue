/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    clearQueueListFn = require('./clear-queue-list');
chai.use(chaiAsPromised);

describe('clear-queue-list.js', function () {
    var queueList = require('../client-queue-list');
    var clearQueueList = clearQueueListFn(queueList);

    beforeEach(function () {
        queueList.createQueue();
        queueList.createQueue();
        queueList.createQueue();
    });

    it("Debe construirse correctamente", function () {
        expect(clearQueueListFn).to.be.a('function');
        expect(clearQueueList).to.be.a('function');
    });

    it("Debe limpiar la cola", function () {
        expect(queueList.members.length).to.be.above(0);
        queueList.clearQueueList();
        expect(queueList.members.length).to.equal(0);
    });

});
