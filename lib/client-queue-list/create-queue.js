/**
 * Created by mbisono on 3/2/16.
 * Crea una
 */
var Queue = require('../client-queue');
var Promise = require("bluebird");

module.exports = function (queueList) {
    return function () {
        return new Promise(function (resolve) {
            if(!queueList){
                throw "La lista de colas es requerida";
            }
            
            var queue = new Queue();
            queueList.members = queueList.members ||[];
            queueList.members.push(queue);
            resolve(queue);
        });
    };
};
