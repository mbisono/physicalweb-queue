/**
 * Created by mbisono on 3/2/16.
 */

var createQueue = require('./create-queue');
var findById = require('./get-queue-by-id');
var clearQueueList = require('./clear-queue-list');

var ClientQueueList = function () {
    this.members = [];
    this.createQueue = createQueue(this);
    this.findById = findById(this);
    this.clearQueueList = clearQueueList(this);
};

module.exports = new ClientQueueList(); //it's a singleton
