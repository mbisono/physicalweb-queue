var shortId = require('shortid');

/**
 * Created by mbisono on 3/2/16.
 * Cliente, corresponde a una persona espera ser atendida en negocio.
 * @class Client
 * @contructor
 */
module.exports = function (name) {
    if (name && typeof name !== 'string') {
        throw "Nombre Invalido";
    }
    /**
     * Id del cliente, este es generado automaticamente.
     * @property id
     * @type {string}
     */
    this.id = shortId.generate();
    /**
     * Nombre del cliente
     * @property name
     * @type {string}
     */
    this.name = name || "Sin nombre";

    /**
     * Fecha de llegada o registro del cliente
     * @type {number}
     * @default Date.now()
     */
    this.arrivedAt = Date.now();
};
