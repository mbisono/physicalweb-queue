/**
 * Created by mbisono on 3/2/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    Client = require('./index');

describe('client', function () {
    
    it('Debe instanciar correctamente', function () {
        expect(Client).to.be.a('function');
        expect(new Client()).to.be.a('object');
    });
    
    it('Debe crear un cliente con el nombre asignado', function () {
        var name = "test name";
        var client = new Client(name);
        expect(client.name).to.equal(name);
    });
    
    it('Debe asignar "Sin nombre" a un cliente si es creado sin nombre', function () {
        var client = new Client();
        expect(client.name).equal('Sin nombre');
    });
    
    it('No debe permitir crear un cliente con un nombre que no sea String', function () {
        var fn = function () {
            return new Client({});
        };
        
        expect(fn).to.throw();
    });
    
    describe('Creación de objeto', function () {
        var client = null;
        
        beforeEach(function () {
            client = new Client("test user");
        });
        
        it('Debe tener un nombre', function () {
            expect(client.name).to.be.a('string');
            expect(client.name.length).to.be.above(0);
        });
        
        it('Debe asignarle un ID al usuario', function () {
            expect(client.id).to.be.a('string');
        });

        it('Debe asignarle una hora de llegada', function () {
            expect(client.arrivedAt).to.be.a('number');
        });
    });
});
