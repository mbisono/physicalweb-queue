/**
 * Created by mbisono on 3/3/16.
 */
var _ = require('lodash');
var sockets = [];

module.exports = {
    io : null,
    put: function (sock) {
        sockets.push(sock.id);
    },
    remove: function (sock) {
        _.remove(sockets, sock.id);
    },
    get: function () {
        return sockets;
    }
};
