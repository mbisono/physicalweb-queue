/**
 * Created by mbisono on 3/11/16.
 */
var apiRoute = require('../api-route'),
    jadeStatic = require('jade-static'),
    express = require("express"),
    path = require('path');

module.exports = function (app, config) {
    "use strict";
    var publicPath = path.join(config.rootPath, "public");

    app.use(jadeStatic(publicPath)); //serve jade as html
    app.use(express.static(publicPath));  //static file routing
    app.use('/api', apiRoute(express.Router()));
};
