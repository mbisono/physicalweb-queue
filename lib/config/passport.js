/**
 * Created by mbiso on 3/15/2016.
 */
var passport = require('passport'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

module.exports = function (config) {
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromHeader('token');
    opts.secretOrKey = config.jwt.secret;
    opts.issuer = config.jwt.issuer;

    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
       if(jwt_payload.isAdmin){
           done(null,jwt_payload);
       }else{
           done({success : false, message: "Not allowed"},false);
       }

    }));

};