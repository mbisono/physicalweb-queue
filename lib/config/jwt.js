/**
 * Created by mbiso on 3/15/2016.
 */
var jwt = require('jsonwebtoken');
module.exports = function (config) {
    return function (body) {
        return jwt.sign(body, config.jwt.secret, {
            issuer: config.jwt.issuer
        });
    };
};