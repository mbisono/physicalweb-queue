/**
 * Created by mbisono on 3/11/16.
 */
var socketHolder = require('../socket-holder');

module.exports = function (io) {
    "use strict";
    socketHolder.io = io;

    io.on('connection', function (socket) {
        socketHolder.put(socket);
        socket.on('disconnect', function () {
            socketHolder.remove(socket);
        });
    });

    return io;
};
