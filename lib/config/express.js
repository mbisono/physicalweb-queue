/**
 * Created by mbisono on 3/11/16.
 */
var bodyParser = require('body-parser'),
    morgan = require('morgan'),
    express = require('express');

module.exports = function (app,config) {
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(morgan('tiny'));
    app.set('port', config.port); //listening port
};
