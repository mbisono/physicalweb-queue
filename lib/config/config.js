/**
 * Created by mbisono on 3/11/16.
 */
var path = require('path'),
    rootPath = path.normalize(__dirname + '/../..');

module.exports = {
    "production": {
        rootPath: rootPath,
        port : process.env.PORT || 80,
        jwt : {
            issuer : "waiterplease.com",
            secret :  process.env.JWT_SECRET
        }
    },
    "development": {
        rootPath: rootPath,
        port : process.env.PORT || 1337,
        jwt : {
            issuer : "waiterplease.com",
            secret : process.env.JWT_SECRET || "secret"
        }
    }
};
