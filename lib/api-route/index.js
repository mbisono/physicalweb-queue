/**
 * Created by mbisono on 3/2/16.
 */

var queueListRoute = require('./queue-list-route');
var queueRoute = require('./queue-route');
var config = require('../config/config')[process.env.NODE_ENV||'development'];

var jwt = require('../config/jwt')(config);
module.exports = function (router) {

    router.post('/login', function (req, res, next) {
        var token = jwt({"isAdmin":true});
        res.status(200).json({token: token});
    });

    router.get(
        '/', function (req, res) {
            res.send('TODO show documentation');
        }
    );

    queueListRoute(router);
    queueRoute(router);

    return router;
};
