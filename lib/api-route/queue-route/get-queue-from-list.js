/**
 * Created by mbisono on 3/2/16.
 */

var queueList = require('../../client-queue-list');

module.exports = function (req, res, next) {
    queueList.findById(req.params.queueId).then(function (queue) {
        req.queue = queue;
        next();
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            message: err.message
        });
    });
};
