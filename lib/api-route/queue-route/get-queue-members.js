/**
 * Created by mbisono on 3/2/16.
 */
module.exports = function (req, res, next) {
    var queue = req.queue;
    res.status(200).json(queue.members);
};
