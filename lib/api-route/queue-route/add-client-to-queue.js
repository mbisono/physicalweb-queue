/**
 * Created by mbisono on 3/2/16.
 */

var Client = require('../../client');
var ioHolder = require('../../../lib/socket-holder');

module.exports = function (req, res) {
    var queue = req.queue;

    console.log(queue);
    return queue.addToQueue(new Client(req.body.name))
        .then(function (client) {
            res.status(200).json(client);
            ioHolder.io.emit('queue:' + queue.id + ':changed', queue);
            return client;
        })
        .catch(function (err) {
            res.status(400).json({
                success: false,
                message: err.message
            });
        });
};
