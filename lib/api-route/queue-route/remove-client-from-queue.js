/**
 * Created by mbisono on 3/2/16.
 */

var ioHolder = require('../../../lib/socket-holder');
module.exports = function (req, res, next) {
    var queue = req.queue;

    queue.removeClientById(req.params.clientId).then(function (queue) {
        res.json({success: true, message: "Eliminado Exitosamente"});
    }).catch(function (err) {
        ioHolder.io.emit('queue:changed', {});
        res.status(400).json({success: false, message: err.message});
    });

};
