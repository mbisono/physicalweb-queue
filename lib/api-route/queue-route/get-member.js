/**
 * Created by mbisono on 3/2/16.
 */
module.exports = function (req, res, next) {
    var member = req.member;
    res.status(200).json(member);
};
