/**
 * Created by mbisono on 3/2/16.
 */
var getQueueById = require('./get-queue-from-list');
var getQueue = require('./get-queue');
var promoteMember = require('./promote-next');
var addMember = require('./add-client-to-queue');
var deleteClient = require('./remove-client-from-queue');
var getQueueMembers = require('./get-queue-members');
var setMemberById = require('./get-member-by-id');
var getMember = require('./get-member');
var deleteQueueMembers = require('./clear-queue');

var passport = require('passport');


module.exports = function (router) {
    router.use(['/queues/:queueId', '/queues/:queueId/*'], getQueueById);

    router.route('/queues/:queueId')
        .get(getQueue);

    router.route('/queues/:queueId/promote')
        .post(passport.authenticate('jwt', { session: false}),promoteMember);

    router.route('/queues/:queueId/members')
        .get(getQueueMembers)
        .post(addMember)
        .delete(deleteQueueMembers);

    router.route('/queues/:queueId/members/:clientId')
        .all(setMemberById)
        .get(getMember)
        .delete(deleteClient);
};
