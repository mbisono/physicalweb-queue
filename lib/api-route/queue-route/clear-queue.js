/**
 * Created by mbisono on 3/2/16.
 */

var ioHolder = require('../../../lib/socket-holder');
module.exports = function (req, res, next) {
    var queue = req.queue;
    queue.clearQueue().then(function (queue) {
        res.status(200).json({success: true, message: "Cola vaciada exitosamente."})
    }).then(function () {
        ioHolder.io.emit('queue:'+queue.id+':changed', queue);
    }).catch(function (err) {
        res.status(400).json({
            success : false,
            message : err.message
        });
    });
};
