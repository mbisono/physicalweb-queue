/**
 * Created by mbisono on 3/2/16.
 */

var ioHolder = require('../../../lib/socket-holder');
module.exports = function (req, res) {
    "use strict";
    var queue = req.queue;

    queue.promoteNext().then(function (queue) {
        if (queue.current.client) {
            ioHolder.io.emit('message:' + queue.id + ':' + queue.current.client.id, {
                title: "Hola,",
                body: "Su turno ha llegado."
            });
        }
        res.status(200).json(queue);
    }).catch(function (err) {
        res.status(400).json({success: false, message: err.message});
    }).finally(function () {
        ioHolder.io.emit('queue:' + queue.id + ':changed', queue);
    });
};
