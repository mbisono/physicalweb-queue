/**
 * Created by mbisono on 3/2/16.
 */
module.exports = function (req, res, next) {
    var queue = req.queue;

    queue.getClientById(req.params.clientId).then(function (member) {
        req.member = member;
        next();
    }).catch(function (err) {
        res.status(400).json({success: false, message: err.message});
    });
};
