/**
 * Created by mbisono on 3/2/16.
 */
var getQueueList = require('./get-queue');
var createQueue = require('./create-queue');
var deleteAllQueues = require('./delete-queues');

module.exports = function (router) {
    router.route('/queues')
        .get(getQueueList)
        .post(createQueue)
        .delete(deleteAllQueues);
};
