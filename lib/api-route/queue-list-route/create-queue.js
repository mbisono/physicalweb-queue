/**
 * Created by mbisono on 3/2/16.
 */

var clientQueueList = require('../../client-queue-list');

module.exports = function (req, res, next) {
    clientQueueList.createQueue().then(function (queue) {
        res.status(201).json(queue.members);
    });
};
