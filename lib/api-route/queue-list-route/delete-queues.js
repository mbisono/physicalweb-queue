/**
 * Created by mbisono on 3/2/16.
 */

var queueList = require('../../client-queue-list');
module.exports = function (req, res, next) {
    queueList.clearQueueList().then(function (queueList) {
       res.status(200).json(queueList); 
    });
};
