/**
 * Created by mbisono on 3/2/16.
 */
var clientQueueList = require('../../client-queue-list');
module.exports = function (req, res, next) {
    res.json(clientQueueList.members);
};
