/**
 * Created by mbisono on 3/2/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    addToQueue = require('./add-to-queue'),
    Queue = require('../client-queue'),
    Client = require('../client');

chai.use(chaiAsPromised);

describe('Add to Queue', function () {
    var queue = new Queue();

    var addToQueueFn = addToQueue(queue);
    
    it('Debe retornar una función', function () {
        expect(addToQueue(queue)).to.be.a('function');
    });
    
    it('No debe permitir agregar un cliente vacio', function (done) {
        addToQueueFn().catch(function (err) {
            expect(err).to.equal("El Cliente requerido");
            done();
        });
    });
    
    describe('Agregar clientes a la cola',function () {
        var testUser = new Client("test user");
        
        it('La cola debe iniciar vacia', function () {
            expect(queue.members.length).to.equal(0);
        });
        
        it('Debe agregar un Cliente a la cola', function () {
            addToQueueFn(testUser);
            expect(queue.members.length).to.equal(1);
            expect(queue.members[0]).to.equal(testUser);
        });
    })
    
});
