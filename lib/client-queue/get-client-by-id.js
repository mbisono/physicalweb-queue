/**
 * Created by mbisono on 3/2/16.
 * Retorna una función que retorna el cliente de la cola a la que corresponda
 * el cliente con el ID indicado.
 * @class Queue
 * @method getClientById
 * @param queue {Object}
 * @returns {Function|Promise|Object} Retorna una promesa que resuelve en el cliente.
 */
var Promise = require('bluebird');
var _ = require('lodash');
module.exports = function (queue) {
    /**
     * @param id {string} Id del cliente a obtener
     */
    return function (id) {
        return new Promise(function (resolve) {
            if (!queue) {
                throw "la cola es requerida";
            }

            if (!id) {
                throw "El id es requerido";
            }

            var member = _.find(queue.members, function (member) {
                return member.id === id;
            });

            if (!member) {
                throw "El cliente no se encuentra en la cola.";
            }

            return resolve(member);
        });
    };
};
