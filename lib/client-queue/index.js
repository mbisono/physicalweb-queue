/**
 * Created by mbisono on 3/2/16.
 * Cola de espera a la cual los clientes pueden unirse para ser atendidos
 * @class Queue
 */
var getCurrent = require('./get-current');
var promoteNext = require('./promote-next');
var addToQueue = require('./add-to-queue');
var clearQueue = require('./clear-queue');
var shortid = require('shortid');
var removeClientById = require('./remove-client-by-id');
var getClientById = require('./get-client-by-id');

module.exports = function () {
    "use strict";
    var queue = this;

    /**
     * Identificador de la cola.
     * @property id 
     * @type {string}
     */
    this.id = shortid.generate();
    /**
     * Cliente actualmente siendo atendido
     * @property current
     * @type {Object}
     */
    this.current = {
        /**
         * Client actualmente siendo atendido.
         * @property client
         * @type {Object}
         */
        client: null
    };

    /**
     * Cola de clientes en espera de ser atendidos.
     * @property members
     * @type {Array|Object}
     */
    this.members = [];
    
    this.getCurrent = getCurrent(queue);
    this.promoteNext = promoteNext(queue);
    this.addToQueue = addToQueue(queue);
    this.clearQueue = clearQueue(queue);
    this.removeClientById = removeClientById(queue);
    this.getClientById = getClientById(queue);
};

