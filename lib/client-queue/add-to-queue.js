/**
 * Created by mbisono on 3/2/16.
 * Retorna una funcion que agrega un cliente a la cola deseada.
 * @class Queue
 * @method addToQueue
 * @param queue
 * @returns {Function}
 */

var Promise = require('bluebird');
module.exports = function (queue) {
    "use strict";
    /**
     * @param client Cliente a ser agregado a la cola
     */
    return function (client) {
        return new Promise(function (resolve) {
            if (!queue) {
                throw "La cola es requerida";
            }
            if (!client) {
                throw "El Cliente requerido";
            }
            queue.members = queue.members || [];
            queue.members.push(client);
            resolve(client);
        });
    };
};
