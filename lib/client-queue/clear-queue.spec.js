/**
 * Created by mbisono on 3/2/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('../client-queue'),
    Client = require('../client');
chai.use(chaiAsPromised);

var clearQueueFn = require('./clear-queue');

describe('Clear queue', function () {
    var queue = new Queue();
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());
    
    var clearQueue = clearQueueFn(queue);
    
    it('Debe instanciar correctamente', function () {
        expect(clearQueueFn).to.be.a('function');
        expect(clearQueue).to.be.a('function');
    })
    
    it('Debe limpiar la cola de clientes', function () {
        expect(queue.members.length).to.be.above(0);
        clearQueue();
        expect(queue.members.length).to.equal(0);
    })
});
