var Promise = require('bluebird');
/**
 * Created by mbisono on 3/2/16.
 * Retorna una función que limpia la cola de espera enviada en el parametro.
 * @class Queue
 * @method clearQueue
 * @param queue
 * @returns {Function|Promise|Object} Retorna función que al ejecutarla retorna una promesa que resuelve en la misma cola afectada.
 */
module.exports = function (queue) {
    return function () {
        return new Promise(function (resolve) {
            queue.members = [];
            resolve(queue);
        });
    };
};
