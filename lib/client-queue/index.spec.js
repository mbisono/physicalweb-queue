/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('./index'),
    Client = require('../client'),
    _ = require('lodash');

chai.use(chaiAsPromised);

describe('client-queue', function () {
    var queue = null;

    beforeEach('Instancia una cola nueva', function () {
        queue = new Queue();
    });

    it("Debe instanciar correctamente", function () {
        expect(Queue).to.be.a('function');
        expect(queue).to.be.a('object');
    });

    describe('La cola debe estar construida correctamente', function () {
        it('members', function () {
            expect(queue.members).to.be.a('array');
            expect(queue.members.length).to.equal(0);
        });

        it('id', function () {
            expect(queue.id).to.be.a('string');
            expect(queue.id.length).to.be.above(0);
        });

        it('current', function () {
            expect(queue.current).to.be.a('object');
        });

        describe('getCurrent()', function () {
            it('Debe traer al cliente actualmente siendo atenido', function (done) {
                expect(queue.getCurrent).to.be.a('function');
                expect(queue.getCurrent()).to.eventually.equal(null).notify(done);
                queue.addToQueue(new Client());
                queue.promoteNext();
                expect(queue.getCurrent()).to.eventually.be.a('object');
            });
        });

        describe('promoteNext()', function () {
            it('Debe promover a la proxima persona en la cola a ser atendido.', function () {
                expect(queue.promoteNext).to.be.a('function');
                var testUser = new Client();
                queue.addToQueue(testUser);
                expect(queue.current.client).to.not.be.equal(testUser);
                expect(queue.members[0]).to.be.equal(testUser);
                queue.promoteNext();
                expect(queue.current.client).to.be.equal(testUser);
            });
        });

        describe("addToQueue()", function () {
            var cliente = null;

            beforeEach("Instancia un cliente de pruebas", function () {
                cliente = new Client();
            });

            it('Debe agregar un cliente a la cola', function (done) {
                expect(queue.members.length).to.equal(0);
                queue.addToQueue(cliente).then(function (rcliente) {
                    expect(rcliente).to.equal(cliente);
                    expect(queue.members[0]).to.equal(cliente);
                    expect(queue.members.length).to.equal(1);
                    done();
                });
            });
        });

        describe("clearQueue()", function () {
            beforeEach(function () {
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
            });

            it("Debe limpiar la cola de espera", function (done) {
                expect(queue.members.length).to.be.above(0);
                queue.clearQueue().then(function (rqueue) {
                    expect(rqueue).to.equal(queue);
                    expect(queue.members.length).to.equal(0);
                    done();
                });
            });
        });

        describe("removeClientById()", function () {
            beforeEach(function () {
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
            });

            it('Debe remover de la cola al cliente con ID indicado', function (done) {
                expect(queue.members.length).to.equal(4);
                var id = queue.members[0].id;
                expect(id).to.be.a('string');
                queue.removeClientById(id).then(function (rqueue) {
                    expect(rqueue).to.equal(queue);
                    expect(queue.members.length).to.equal(3);
                    expect(_.find(queue.members, function (member) {
                        return member.id === id;
                    })).to.equal(undefined);
                    done();
                });
            });
        });

        describe("getClientById()", function () {
            beforeEach(function () {
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
                queue.addToQueue(new Client());
            });
            
            it("Debe traer el cliente con id indicado", function (done) {
                expect(queue.members.length).to.equal(4);
                var id = queue.members[2].id;
                expect(id).to.be.a('string');
                
                expect(queue.getClientById(id)).to.eventually.equal(queue.members[2]).notify(done);
            });
        });
    });

});
