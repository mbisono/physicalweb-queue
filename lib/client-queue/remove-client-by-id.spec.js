/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('../client-queue'),
    Client = require('../client'),
    removeClientByIdFn = require('./remove-client-by-id'),
    _ = require('lodash');

chai.use(chaiAsPromised);

describe('remove-client-by-id.js', function () {
    var queue = new Queue(),
        testClient = new Client('a');
    queue.addToQueue(testClient);
    queue.addToQueue(new Client('b'));
    queue.addToQueue(new Client('c'));
    queue.addToQueue(new Client('d'));

    var removeClientById = removeClientByIdFn(queue);

    it('Debe instanciar correctamente', function () {
        expect(removeClientByIdFn).to.be.a('function');
        expect(removeClientById).to.be.a('function');
    });

    it('Debe remover el elemento con el Id pasado como parametro', function (done) {
        expect(queue.members.length).to.equal(4);
        removeClientById(testClient.id).then(function () {
            expect(queue.members.length).to.equal(3);
            expect(_.find(queue.members, function (member) {
                return member.id === testClient.id;
            })).to.equal(undefined);
            done();
        });
    });

    it('Debe retornar un error si se busca un id no existente', function (done) {
        expect(removeClientById('z')).to.eventually.be.rejected.notify(done);
    })
});
