/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('../client-queue'),
    Client = require('../client'),
    getClientByIdFn = require('./get-client-by-id');

chai.use(chaiAsPromised);

describe("get-client-by-id.js", function () {
    var queue = new Queue();
    var testClient = new Client("test");
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());
    queue.addToQueue(testClient);
    queue.addToQueue(new Client());
    queue.addToQueue(new Client());

    var getClientById = getClientByIdFn(queue);

    it('Debe instanciar correctamente', function () {
        expect(getClientByIdFn).to.be.a('function');
        expect(getClientById).to.be.a('function');
    });

    it('Debe retornar el cliente de la cola el ID indicado', function (done) {
        expect(getClientById(testClient.id)).to.eventually.equal(testClient).notify(done);
    });

    it('Debe dar error si el cliente no se encuentra en la cola', function (done) {
        expect(getClientById("XXXXXXXXXXXXXXXXX")).to.eventually.be.rejected.notify(done);
    });
    
});
