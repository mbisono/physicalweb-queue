/**
 * Created by mbisono on 3/2/16.
 * Remueve de la cola indicada el cliente con el ID tambien indicado.
 * @class Queue
 * @method removeClientById
 * @param queue {Object} cola de la cual será removido el cliente.
 * @returns{Function, Promise, Object} Retorna la misma cola modificada por este metodo.
 */
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function (queue) {
    /**
     * @param id {string} Id del cliente a remover de la cola.
     */
    return function (id) {
        return new Promise(function (resolve) {
            if (!queue) {
                throw "La cola es requerida";
            }

            if (!id) {
                throw "El id es requerido";
            }

            if (_.remove(queue.members, function (member) {
                    return member.id === id;
                }).length) {
                resolve(queue);
            } else {
                throw "El cliente no existe";
            }
        });
    }
};
