/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('../client-queue'),
    Client = require('../client'),
    promoteNextFn = require('./promote-next');
chai.use(chaiAsPromised);

describe('promote-next.js', function () {
    var queue = new Queue();
    queue.addToQueue(new Client('juan'));
    queue.addToQueue(new Client('pedro'));

    var promoteNext = promoteNextFn(queue);

    it('Debe instanciar correctamente', function () {
        expect(promoteNextFn).to.be.a('function');
        expect(promoteNext).to.be.a('function');
    });

    it('Debe promover al primer cliente de la cola a ser atendido', function () {
        expect(queue.current.client).to.equal(null);
        expect(queue.members.length).to.equal(2);
        promoteNext();
        expect(queue.current.client.name).to.equal('juan');
        expect(queue.members.length).to.equal(1);
        promoteNext();
        expect(queue.current.client.name).to.equal('pedro');
        expect(queue.members.length).to.equal(0);
    });

    it('Si se intenta promover cuando no hay mas clientes en la cola, debe liberar al cliente atendido', function () {
        promoteNext();
        expect(queue.current.client).to.equal(null);
        expect(queue.members.length).to.equal(0);
    });

    it('Debe retornar la misma cola', function (done) {
        expect(promoteNext()).eventually.equal(queue).notify(done);
    })
});
