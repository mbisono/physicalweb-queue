/**
 * Created by mbisono on 3/2/16.
 * Retorna al cliente que esta siendo atendido en la cola actualmente.
 * @class Queue
 * @method getCurrent
 * @param queue
 * @returns {Function|Promise|Object}
 */
var Promise = require('bluebird');

module.exports = function (queue) {
    return function () {
        return new Promise(function (resolve) {
            if (!queue) {
                throw "La cola es requerido";
            }

            resolve(queue.current.client);
        });
    }
};
