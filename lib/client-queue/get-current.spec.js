/**
 * Created by mbisono on 3/9/16.
 */
var chai = require('chai'),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),
    Queue = require('../client-queue'),
    Client = require('../client'),
    getCurrentFn = require('./get-current');

chai.use(chaiAsPromised);

describe('get-current.js', function () {
    var queue = new Queue();
    queue.addToQueue(new Client("test current"));
    queue.promoteNext();
    
    var getCurrent = getCurrentFn(queue);
    
    it("Debe instanciar correctamente", function () {
        expect(getCurrentFn).to.be.a('function');
    });

    describe('Debe retornar el cliente siendo atendido', function () {
        var cliente = getCurrent();


        it("El cliente debe ser un objeto", function (done) {

            expect(cliente).to.eventually.be.a('object').notify(done);
        });

        it("debe corresponder al actualmente atendido", function (done) {
            expect((function () {
                return cliente.then(function (cliente) {
                    return cliente.name;
                })
            })()).eventually.equal("test current").notify(done);
        });
    });
});
