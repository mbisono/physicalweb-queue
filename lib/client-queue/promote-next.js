/**
 * Created by mbisono on 3/2/16.
 * Promueve al siguiente cliente en la cola a ser atendido y libera al cliente actual.
 * @class Queue
 * @method promoteNext
 * @param queue {Object}
 * @returns {Function, Promise, Object} Retorna una funcion que al ser invocada retorna una promesa que resulve en la misma cola afectada.
 */
var Promise = require("bluebird");
module.exports = function (queue) {
    return function () {
        return new Promise(function (resolve) {
            if (!queue) {
                throw "La cola es requerida";
            }
            
            queue.current.client = queue.members.length ? queue.members.shift() : null;
            resolve(queue);
        });
    };
};
