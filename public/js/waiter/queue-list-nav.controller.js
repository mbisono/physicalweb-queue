/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function (socket, $scope) {
    "use strict";
    var vm = this;

    console.log(socket);
    vm.$onInit = function () {
        vm.queueList.forEach(function (queue, index, arr) {
            socket.on('queue:' + queue.id + ':changed', function (queue) {
                arr[index] = queue;
            }).bindTo($scope);
            
        });
    };
};

module.exports.$inject = ['socket', '$scope'];
