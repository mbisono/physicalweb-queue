/**
 * Created by mbisono on 3/3/16.
 * Directiva que construye la lista de espera de clientes
 */
module.exports = {
    bindings: {
        queue : '<',
        queueId : '<'
    },
    templateUrl: 'js/waiter/waiting-list.view.html',
    controller : 'waitingListController',
    controllerAs : 'vm'
};
