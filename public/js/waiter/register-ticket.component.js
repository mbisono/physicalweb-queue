/**
 * Created by mbisono on 3/7/16.
 */
module.exports = {
    bindings : {
        queueId : '<',
        waiterId : '<'
    },
    templateUrl : 'js/waiter/register-ticket.view.html',
    controllerAs : 'vm',
    controller : 'registerTicketController'
};
