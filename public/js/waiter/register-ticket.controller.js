/**
 * Created by mbisono on 3/7/16.
 */
var _ = require('lodash');
module.exports = function (socket, Queue, NotificationSvc, $scope) {
    "use strict";
    var vm = this;
    vm.status = "waiting";

    function setWaiter(queue, waiterId) {
        var waiter = _.find(queue.members, {id: waiterId});
        if (waiter) {
            vm.waiter = waiter;
            vm.status = "waiting";
            vm.position = _.indexOf(queue.members, vm.waiter);
        } else if (!waiter && queue.current.client !== null && queue.current.client.id === waiterId) {
            vm.waiter = queue.current.client;
            vm.status = "current";
            vm.position = -1;
        } else {
            vm.waiter = {};
            vm.status = "error";
        }
    }

    vm.$onInit = function () {
        Queue(vm.queueId).get().then(function (queue) {
            setWaiter(queue, vm.waiterId);
        });


        socket.on('queue:' + vm.queueId + ':changed', function (queue) {
            setWaiter(queue, vm.waiterId);
        }).bindTo($scope);

        socket.on('message:' + vm.queueId + ':' + vm.waiterId, function (message) {
            NotificationSvc(message.title, message.body);
        });
    };
};

module.exports.$inject = ['socket', 'Queue', 'NotificationSvc', '$scope'];
