/**
 * Created by mbisono on 3/3/16.
 * Modulo principal de la aplicación.
 * Define los requisitos.
 */


require('ng-socket-io/build/ng-socket-io.min');

module.exports = angular.module('WaiterApp', [
    'ui.router', 'restangular', 'socket-io'
]);
