/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function ($stateProvider, $urlRouterProvider) {
    "use strict";
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('index', {
            url: '/',
            template: '<queue-list-nav data-queue-list="vm.queueList"></queue-list-nav>',
            controllerAs: 'vm',
            controller: function (queueList) {
                this.queueList = queueList;
            },
            resolve: {
                queueList: ['QueueList', function (QueueList) {
                    return QueueList.getList();
                }]
            }
        })
        .state('queue', {
            url: "/queue/{queueId}",
            template: '<ui-view></ui-view>',
            abstract: true,
            resolve: {
                queue: ['Queue', '$stateParams', function (Queue, $stateParams) {
                    return Queue($stateParams.queueId);
                }]
            }
        })
        .state('queueView', {
            url: '',
            parent: 'queue',
            template: '<queue-page data-queue="vm.queue"></queue-page>',
            abstract: true,
            controller: ['queue', function (queue) {
                this.queue = queue;
            }],
            resolve: {
                queue: ['queue', function (queue) {
                    return queue.get();
                }]
            },
            controllerAs: 'vm'
        })
        .state('register', {
            url: '/register',
            parent: 'queue',
            controllerAs: 'vm',
            controller: 'registerController',
            template: '<waiter-form data-on-submit="vm.postWaiter(waiter)"><waiter-form/>'
        })
        .state('waiter', {
            url: '/waiter/{waiterId}',
            parent: 'queue',
            controllerAs: 'vm',
            controller: ['$stateParams', function ($stateParams) {
                var vm = this;
                vm.queueId = $stateParams.queueId;
                vm.waiterId = $stateParams.waiterId;
            }],
            resolve: {
                queue: ['queue', function (queue) {
                    return queue.get();
                }]
            },
            template: '<register-ticket data-queue-id="vm.queueId" data-waiter-id="vm.waiterId"></register-ticket>'
        })
        .state('queueRegister', {
            url: '',
            parent: 'queueView',
            template: '<waiter-form data-on-submit="vm.postWaiter(waiter)"></waiter-form>'
        })
        .state('manager', {
            url: '/manager',
            parent: 'queueView',
            controllerAs: 'vm',
            template: '<queue-manager data-queue="vm.queue"></queue-manager>',
            controller: ['queue', function (queue) {
                "use strict";
                var vm = this;
                vm.queue = queue;
            }],
            resolve: {
                queue: ['queue', function (queue) {
                    return queue.get();
                }]
            }
        });
};

module.exports.$inject = ['$stateProvider', '$urlRouterProvider'];
