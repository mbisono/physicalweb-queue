/**
 * Created by mbisono on 3/10/16.
 */
module.exports = {
    bindings: {
        waiter: "<"
    },
    template: "<span ng-bind='vm.waiter.name'></span>",
    controllerAs: 'vm'
};
