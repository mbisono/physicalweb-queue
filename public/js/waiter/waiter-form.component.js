/**
 * Created by mbisono on 3/3/16.
 */
module.exports = {
    bindings : {
        onSubmit : '&'
    },
    controller : "waiterFormController",
    controllerAs : "vm",
    templateUrl : "js/waiter/waiter-form.view.html"
};
