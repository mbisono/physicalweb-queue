/**
 * Created by mbisono on 3/3/16.
 */
module.exports = {
    bindings : {
        queueList : '<'
    },
    controllerAs : 'vm',
    controller : 'queueListNavController',
    templateUrl : 'js/waiter/queue-list-nav.view.html'
};
