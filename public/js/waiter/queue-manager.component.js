/**
 * Created by mbisono on 3/10/16.
 */
module.exports = {
    bindings : {
        queue : '<'
    },
    controllerAs : 'vm',
    controller : 'queueManagerController',
    templateUrl : 'js/waiter/queue-manager.view.html'
};
