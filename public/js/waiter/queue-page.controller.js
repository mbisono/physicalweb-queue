/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function () {
    var vm = this;

    vm.postWaiter = function (waiter) {
        return vm.queue.all('members').post(waiter);
    }
};

module.exports.$inject = [];
