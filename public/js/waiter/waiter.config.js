/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function (RestangularProvider) {
    RestangularProvider.setBaseUrl('/api');
    RestangularProvider.addRequestInterceptor(function(element, operation, what, url) {
        return element;
    })
};

module.exports.$inject = ['RestangularProvider'];
