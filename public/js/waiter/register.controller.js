/**
 * Created by mbisono on 3/7/16.
 */
module.exports = function (queue, $state, $stateParams) {
    var vm = this;
    vm.postWaiter = function (waiter) {
        return queue.all('members')
            .post(waiter)
            .then(function (data) {
                $state.go('waiter', {'waiterId': data.id, 'queueId': $stateParams.queueId})
            })
    }
};

module.exports.$inject = ['queue', '$state', '$stateParams'];
