/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function (Restangular) {
    return Restangular.service('queues');
};

module.exports.$inject = ['Restangular'];
