/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function (socket,$scope) {
    "use strict";
    var vm = this;

    vm.$onInit = function () {
        socket.on('queue:' + vm.queueId + ':changed', function (queue) {
            vm.queue = queue;
        }).bindTo($scope);
    };
};

module.exports.$inject = ['socket','$scope'];
