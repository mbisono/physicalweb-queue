/**
 * Created by mbisono on 3/3/16.
 */
module.exports = {
    bindings : {
        queue : '<'
    },
    controller : 'queuePageController',
    controllerAs : 'vm',
    templateUrl : 'js/waiter/queue-page.view.html'
};
