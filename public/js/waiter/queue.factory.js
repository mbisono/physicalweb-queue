/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function (QueueList) {
    return function (queueId) {
        return QueueList.one(queueId);
    }
};

module.exports.$inject = ['QueueList'];
