/**
 * Created by mbisono on 3/3/16.
 */
module.exports = function () {
    "use strict";
    var vm = this;
    vm.nombre = "";

    vm.submit = function () {
        vm.onSubmit({waiter: {name: vm.nombre}})
            .then(function () {
                vm.nombre = "";
            })
            .catch(function (err) {
                console.log('error: ', err)
                alert('No pudiste ser agregado a la lista');
            });
    };
};

module.exports.$inject = [];
