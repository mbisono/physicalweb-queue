/**
 * Created by mbisono on 3/3/16.
 * Construye la aplicación
 */

var app = require('./waiter.module');

app.config(require('./waiter.config'));
app.config(require('./waiter.route'));
app.controller('appController',require('./app.controller'));
app.component('waitingList', require('./waiting-list.component'));
app.controller('waitingListController', require('./waiting-list.controller'));
app.component('waiterForm',require('./waiter-form.component'));
app.controller('waiterFormController',require('./waiter-form.controller'));
app.component("siteNavbar",require('./navbar.component'));
app.controller("siteNavbarController",require('./navbar.controller'));
app.factory('QueueList',require('./queue-list.factory'));
app.factory('Queue',require('./queue.factory'));
app.component('queuePage',require('./queue-page.component.js'));
app.controller('queuePageController',require('./queue-page.controller.js'));
app.component('queueListNav',require('./queue-list-nav.component'));
app.controller('queueListNavController',require('./queue-list-nav.controller'));
app.component('registerTicket',require('./register-ticket.component'));
app.controller('registerTicketController',require('./register-ticket.controller'));
app.controller('registerController',require('./register.controller'));
app.factory('NotificationSvc',require('./notification.factory'));
app.component('waiterName',require("./waiter-name.component"));
app.component('waiterPosition',require('./waiter-position.component'));
app.component('queueManager',require('./queue-manager.component'));
app.controller('queueManagerController',require('./queue-manager.controller'));
