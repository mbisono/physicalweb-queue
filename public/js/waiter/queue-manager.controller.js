/**
 * Created by mbisono on 3/10/16.
 */
module.exports = function (socket, Queue, $scope) {
    "use strict";
    var vm = this;


    vm.promoteNext = function () {
        Queue(vm.queue.id).customPOST({}, 'promote')
            .then(function () {
                console.log("success");
            })
            .catch(function (err) {
                console.log("fail: ", err);
            });
    };


    vm.$onInit = function () {
        socket.on('queue:' + vm.queue.id + ':changed', function (queue) {
            vm.queue = queue;
        }).bindTo($scope);
    };


};

module.exports.$inject = ['socket','Queue','$scope'];
