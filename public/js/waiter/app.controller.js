/**
 * Created by mbisono on 3/7/16.
 */
module.exports = function ($rootScope) {
    "use strict";
    $rootScope.$on('$stateChangeError',
        function (event, toState, toParams, fromState, fromParams, error) {
            console.log(event, error);
        });
};

module.exports.$inject = ['$rootScope'];
