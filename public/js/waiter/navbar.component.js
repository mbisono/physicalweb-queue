/**
 * Created by mbisono on 3/3/16.
 */
module.exports = {
    bindings : {},
    controller : "siteNavbarController",
    controllerAs : "vm",
    templateUrl : "js/waiter/navbar.view.html"
};
