/**
 * Created by mbisono on 3/10/16.
 */
module.exports = {
    bindings: {index: '<'},
    controllerAs: 'vm',
    template: "<ng-pluralize count='vm.index' when=\"{'0':'Próximo', '1':'3', 'other':'{{vm.index + 2}}'}\"></ng-pluralize>"
};
