/**
 * Created by mbisono on 3/8/16.
 */
module.exports = function () {
    if (("Notification" in window) && Notification.permission !== 'denied') {
        Notification.requestPermission();
    }
    return function notifyMe(title, body, icon) {
        if (!("Notification" in window)) {
            alert(body);
        }
        else if (Notification.permission === "granted") {
            var options = {
                body: body,
                icon: icon,
                dir: "ltr"
            };
            var notification = new Notification(title, options);
        }
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if (!('permission' in Notification)) {
                    Notification.permission = permission;
                }

                if (permission === "granted") {
                    var options = {
                        body: body,
                        icon: icon,
                        dir: "ltr"
                    };
                    var notification = new Notification(title, options);
                }
            });
        }
    };
};
    
    
