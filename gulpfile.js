var gulp = require('gulp');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('scripts', function () {
    return buildScript('app.js', false);
});

gulp.task('watch', function () {
    return buildScript('app.js', true);
});

function buildScript(file, watch) {
    var props = {
        entries: ['public/js/app.js'],
        debug: true,
        cache: {},
        packageCache: {}
    };

    var bundler = watch ? watchify(browserify(props)) : browserify(props);

    function rebundle() {
        var stream = bundler.bundle();
        return stream.on('error', console.log.bind(console))
            .pipe(source(file))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('public/build'));
    }

    bundler.on('update', function () {
        rebundle();
        gutil.log('Rebundle...');
    });

    bundler.on('log', gutil.log); // output build logs to terminal

    return rebundle();
}



gulp.task('default', ['scripts']);
