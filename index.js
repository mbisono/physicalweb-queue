var express = require('express'),
    http = require('http'),
    socketIo = require('socket.io'),
    interfaces = require('./lib/get-interfaces'),
    env = process.env.NODE_ENV || "development",
    config = require('./lib/config/config')[env];


//express app
var app = express(); //Express app
require('./lib/config/express')(app,config);

//Creates the server
var server = http.createServer(app);


require('./lib/config/passport')(config);

//socket.io
var io = socketIo.listen(server);
require('./lib/config/socket')(io);
app.io = io;

//routes
require('./lib/config/routes')(app,config); //routes

//inicializa la aplicacion
require('./lib/config/app')();

//start server
server.listen(app.get('port'), function () {
    //display access points
    console.log('listening on http://localhost' + ':' + app.get('port'));
    interfaces.forEach(function (inf) {
        console.log('listening on http://' + inf + ':' + app.get('port'));
    });
}); 
